# JOI Writeup

## Challenge description

All we have is just one image

## And now what?

When we deal with images / audio / videos, the first thing we have to do, is to analyse it by using some basic steganography tools, such as [StegSolve](http://www.caesum.com/handbook/Stegsolve.jar)
## Analysing the image

If you use Stegsolve, you have to execute the .jar file and import the result.png file.

> java -jar StegSolve.jar

By using the arrows inside the GUI, you can change the picture plane and can observe different images or something similar to an image. By selecting the "Red Plane 0", you can see a different QR Code, scan it with your mobile device or zbarimg and get that juicy and easy flag.

> VolgaCTF{5t3g0_m4tr3shk4_in_4cti0n}

## Little rabbit hole where I fell in

If you try to user zbarimg on result.png, you will get the following output:
> C_F(n1, n2) = 14 * [C(n1,n2) / 14] + 7 * FLAG(n1,n2) + (C(n1,n2) mod 7)

I wasted a lot trying to figure it out what to do and what the hell does this string mean and in the end, I lost a complete half a hour for no reason.
