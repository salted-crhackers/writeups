# Shop

## Challenge description
There is an online shop. We can register, log in and make purchases. The goal is to buy the flag item, which costs $1337, but we have only a balance of $100.

## Obtaining the code - robots.txt

By looking at `/robots.txt` we get:
```
Disallow: /shop-1.0.0.war
```
We download the [WAR archive](https://github.com/salted-crhackers/writeups/raw/master/2019/volgactf-qualifier/shop/shop-1.0.0.war), then decompile it. JD-GUI fails to decompile the most interesting file, which is ShopController (the controller of the Spring app). Intellij IDEA's decompiler does the job: [ShopController.java](https://github.com/salted-crhackers/writeups/blob/master/2019/volgactf-qualifier/shop/ShopController.java)

## Analyzing the code

The app seems a straightforward MVC Spring app. The code of the buy function in the controller doesn't seem to contain particular vulnerabilities.
We notice this part in the ShopController:
```
@InitBinder
public void initBinder(WebDataBinder binder) {
	binder.setDisallowedFields(new String[]{"balance"});
}
```
This prevents the binding of the field `balance` in the model, thus not letting a parameter `balance` in the HTTP requests to overwrite the value of field `balance` of the `User` in the model.

## Controlling the model

Fortunately,`balance` is not the only field we are interested in. `User` also has a `private List<Product> cart;` attribute, with corresponding getter `public List<Product> getCartItems()`, which represents the items bought by the user.
Since we can use array and dot notations to access fields and their attributes, we force the first item of the `cart` collection to have `id` equal to 4 (the ID of the flag product).
We do this while simultaneously buying an item we can affort (the t-shirt, ID 1), so that the model is persisted afterwards by the buy action in the ShopController (`this.userDao.update(user);`)

We first retrieve our session from the cookie in the browser, and then send the following request with curl:
```
curl --verbose --data 'cartItems[0].id=4&productId=1' -H "Cookie: JSESSIONID=AFF141673D7E2118EF11F3B36FE50E30" http://shop.q.2019.volgactf.ru/buy
```
We then return to the browser, and view our profile. We bought the flag.

## A simpler solution

A solution I did **not** notice: `setDisallowedFields` apparently is case sensitive, but the binding with POST variables is not. So requests like this
```
http://shop.q.2019.volgactf.ru/index?Balance=10000
```
just do the trick. Combine with the buy functionality and you suddenly have the sufficient balance to buy everything. I think this was clearly the intended solution, because it wouldn't work for [Shop V.2](https://github.com/salted-crhackers/writeups/tree/master/2019/volgactf-qualifier/shop-v2), while mine did. Oh well! I guess two flags at the price of one isn't that bad.
